{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE OverloadedStrings #-}

module Model.OrderItem where

import           Import
import           Data.Text                            (Text)


-- | Represents the size of a t-shirt (PP, P, M, G, etc) and its style
-- (traditional, baby look, etc).
-- Eg.: T-PP (small traditional shirt), BL-XL (extra large baby look shirt), and
-- so on.
type SizeStyle = Text


data OrderItem = OrderItem
    { orderItemOrderId   :: Int64
    , orderItemShirtId   :: Int64
    , orderItemSizeStyle :: Text
    , orderItemQuantity  :: Int
    }
  deriving (Eq, Show)


instance FromRow OrderItem where
    fromRow = OrderItem
        <$> _orderItemOrderId
        <*> _orderItemShirtId
        <*> _orderItemSizeStyle
        <*> _orderItemQuantity
      where
        !_orderItemOrderId   = field
        !_orderItemShirtId   = field
        !_orderItemSizeStyle = field
        !_orderItemQuantity  = field

