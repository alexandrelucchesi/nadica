<bind tag="custom-css">
  <link type="text/css" rel="stylesheet" href="/css/register.css" />
</bind>
<apply template="_base">
<div class="container" ng-controller="RegisterCtrl">
  <div class="page-header">
    <h1>Cadastro</h1>
  </div>
  <form name="registerForm" class="container" role="form" ng-submit="register(registerForm.$valid)" novalidate>
    <fieldset class="col-md-6">
      <legend>Dados Pessoais</legend>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.name.$invalid && submitted,
                      'has-success' : registerForm.name.$valid && submitted }">
        <label for="name">Nome</label>
        <input id="name" name="name" type="text" class="form-control"
            ng-model="user.userName" placeholder="Digite seu nome" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.name.$invalid && submitted,
                      'glyphicon-ok' : registerForm.name.$valid && submitted }"></span>
        <p ng-show="registerForm.name.$invalid && submitted" class="help-block">O nome digitado é inválido.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.surname.$invalid && submitted,
                      'has-success' : registerForm.surname.$valid && submitted }">
        <label for="surname">Sobrenome</label>
        <input id="surname" name="surname" type="text" class="form-control"
            ng-model="user.userSurname" placeholder="Digite seu sobrenome" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.surname.$invalid && submitted,
                      'glyphicon-ok' : registerForm.surname.$valid && submitted }"></span>
        <p ng-show="registerForm.surname.$invalid && submitted" class="help-block">O sobrenome digitado é inválido.</p>              
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.birthday.$invalid && submitted,
                      'has-success' : registerForm.birthday.$valid && submitted }">
        <label for="birthday">Data de Nascimento</label>
        <input id="birthday" name="birthday" type="text" class="form-control"
            ng-model="user.userBirthday" ui-mask="99/99/9999" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.birthday.$invalid && submitted,
                      'glyphicon-ok' : registerForm.birthday.$valid && submitted }"></span>
        <p ng-show="registerForm.birthday.$invalid && submitted" class="help-block">Coloque uma data de nascimento válida.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.gender.$invalid && submitted,
                      'has-success' : registerForm.gender.$valid && submitted }">
        <label for="gender">Sexo</label>
        <select id="gender" name="gender" class="form-control"
            ng-model="user.userGender" ng-options="g.code as g.gender for g in genders" required>
        </select>
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.gender.$invalid && submitted,
                      'glyphicon-ok' : registerForm.gender.$valid && submitted }"></span>
        <p ng-show="registerForm.gender.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.rg.$invalid && submitted,
                      'has-success' : registerForm.rg.$valid && submitted }">
        <label for="rg">RG</label>
        <input id="rg" name="rg" type="text" class="form-control"
            ng-model="user.userRg" placeholder="Digite seu RG" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.rg.$invalid && submitted,
                      'glyphicon-ok' : registerForm.rg.$valid && submitted }"></span>
        <p ng-show="registerForm.rg.$invalid && submitted" class="help-block">Insira seu RG.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.cpf.$invalid && submitted,
                      'has-success' : registerForm.cpf.$valid && submitted }">
        <label for="cpf">CPF</label>
        <input id="cpf" name="cpf" type="text" class="form-control"
            ng-model="user.userCpf" ui-mask="999.999.999-99" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.cpf.$invalid && submitted,
                      'glyphicon-ok' : registerForm.cpf.$valid && submitted }"></span>
        <p ng-show="registerForm.cpf.$invalid && submitted" class="help-block">CPF inválido.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.phone.$invalid && submitted,
                      'has-success' : registerForm.phone.$valid && submitted }">
        <label for="phone">Telefone</label>
        <input id="phone" name="phone" type="text" class="form-control"
            ng-model="user.userPhone" ui-mask="(99) 99999999?9" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.phone.$invalid && submitted,
                      'glyphicon-ok' : registerForm.phone.$valid && submitted }"></span>
        <p ng-show="registerForm.phone.$invalid && submitted" class="help-block">Verifique a quantidade de digitos./Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.email.$invalid && submitted,
                      'has-success' : registerForm.email.$valid && submitted }">
        <label for="email">E-mail</label>
        <input id="email" name="email" type="email" class="form-control"
            ng-model="user.userEmail" placeholder="Digite seu e-mail" required>
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.email.$invalid && submitted,
                      'glyphicon-ok' : registerForm.email.$valid && submitted }"></span>
        <p ng-show="registerForm.email.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.password.$invalid && submitted,
                      'has-success' : registerForm.password.$valid && submitted }">
        <label for="password">Senha</label>
        <input id="password" name="password" type="password"
            class="form-control" ng-model="user.userPassword"
          placeholder="Digite sua senha (mínimo 6 caracteres)" required>
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.password.$invalid && submitted,
                      'glyphicon-ok' : registerForm.password.$valid && submitted }"></span>
        <p ng-show="registerForm.email.$invalid && submitted" class="help-block">Verifique se sua senha possui 8 caracteres./É necessário a presença de letras e símbolos./Este campo é obrigatório.</p>              
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.passwordConfirmation.$invalid && submitted,
                      'has-success' : registerForm.passwordConfirmation.$valid && submitted }">
        <label for="passwordConfirmation">Confirmação de senha</label>
        <input id="passwordConfirmation" name="passwordConfirmation"
            type="password" class="form-control" ng-model="user.userPasswordConfirmation" placeholder="Digite novamente sua senha" required>
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.passwordConfirmation.$invalid && submitted,
                      'glyphicon-ok' : registerForm.passwordConfirmation.$valid && submitted }"></span>
        <p ng-show="registerForm.passwordConfirmation.$invalid && submitted" class="help-block">Sua confirmação de senha não confere./Este campo é obrigatório.</p>
      </div>
    </fieldset>
    <fieldset class="col-md-6">
      <legend>Dados de Entrega</legend>
      <div class="form-group required">
        <label for="country">País</label>
        <p id="country" class="form-control-static">{{user.userCountry}}</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.state.$invalid && submitted,
                      'has-success' : registerForm.state.$valid && submitted }">
        <label for="state">Estado</label>
        <select id="state" name="state" class="form-control" ng-model="user.userState" ng-options="s.code as s.state for s in states" required>
        </select>
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.state.$invalid && submitted,
                      'glyphicon-ok' : registerForm.state.$valid && submitted }"></span>
        <p ng-show="registerForm.state.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.city.$invalid && submitted,
                      'has-success' : registerForm.city.$valid && submitted }">
        <label for="city">Cidade</label>
        <input id="city" name="city" type="text" class="form-control"
            ng-model="user.userCity" placeholder="Digite sua cidade" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.city.$invalid && submitted,
                      'glyphicon-ok' : registerForm.city.$valid && submitted }"></span>
        <p ng-show="registerForm.city.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.district.$invalid && submitted,
                      'has-success' : registerForm.district.$valid && submitted }">
        <label for="district">Bairro</label>
        <input id="district" name="district" type="text" class="form-control"
            ng-model="user.userDistrict" placeholder="Digite seu bairro" required />
        <span class="glyphicon form-control-feedback"
        ng-class="{ 'glyphicon-remove' : registerForm.district.$invalid && submitted,
                      'glyphicon-ok' : registerForm.district.$valid && submitted }"></span>
        <p ng-show="registerForm.district.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.address.$invalid && submitted,
                      'has-success' : registerForm.address.$valid && submitted }">
        <label for="address">Endereço</label>
        <input id="address" name="address" type="text" class="form-control"
            ng-model="user.userAddress" placeholder="Digite seu endereço" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.address.$invalid && submitted,
                      'glyphicon-ok' : registerForm.address.$valid && submitted }"></span>
        <p ng-show="registerForm.address.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group has-feedback"
          ng-class="{ 'has-error' : registerForm.complement.$invalid && submitted,
                      'has-success' : registerForm.complement.$valid && submitted }">
        <label for="complement">Complemento</label>
        <input id="complement" name="complement" type="text"
            class="form-control" ng-model="user.userComplement" placeholder="Digite o complemento" />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.complement.$invalid && submitted,
                      'glyphicon-ok' : registerForm.complement.$valid && submitted }"></span>
        <p ng-show="registerForm.complement.$invalid && submitted" class="help-block"></p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.number.$invalid && submitted,
                      'has-success' : registerForm.number.$valid && submitted }">
        <label for="number">Número</label>
        <input id="number" name="number" type="text" class="form-control"
            ng-model="user.userNumber" placeholder="Digite o número" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.number.$invalid && submitted,
                      'glyphicon-ok' : registerForm.number.$valid && submitted }"></span>
        <p ng-show="registerForm.number.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group required has-feedback"
          ng-class="{ 'has-error' : registerForm.zipCode.$invalid && submitted,
                      'has-success' : registerForm.zipCode.$valid && submitted }">
        <label for="zipCode">CEP</label>
        <input id="zipCode" name="zipCode" type="text" class="form-control"
            ng-model="user.userZipCode" ui-mask="99.999-999" required />
        <span class="glyphicon form-control-feedback"
          ng-class="{ 'glyphicon-remove' : registerForm.zipCode.$invalid && submitted,
                      'glyphicon-ok' : registerForm.zipCode.$valid && submitted }"></span>
        <p ng-show="registerForm.zipCode.$invalid && submitted" class="help-block">Este campo é obrigatório.</p>
      </div>
      <div class="form-group">
        <div class="checkbox">
          <label>
            <input type="checkbox"> Desejo receber ofertas exclusivas e os últimos lançamentos por e-mail
          </label>
        </div>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Cadastrar</button>
        <button type="button" class="btn btn-default">Cancelar</button>
      </div>
    </fieldset>
  </form>
</div>
</apply>

