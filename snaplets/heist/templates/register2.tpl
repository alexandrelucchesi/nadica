<apply template="_base">
<div class="container">
  <h1 class="text-center">Cadastro</h1>
  <hr />
  <form class="form-horizontal" role="form">
    <fieldset>
      <div class="row form-group">
        <div class="col-md-6">
          <label for="name" class="control-label">Nome</label>
          <input type="text" class="form-control" id="name" placeholder="Digite seu nome" />
        </div>
        <div class="col-md-6">
          <label for="surname" class="control-label">Sobrenome</label>
          <input type="text" class="form-control" id="name" placeholder="Digite seu sobrenome" />
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-3">
          <label for="country" class="control-label">País</label>
          <p class="form-control-static">Brasil</p>
        </div>
        <div class="col-md-3">
          <label for="country" class="control-label">Estado</label>
          <select class="form-control">
            <option value="">Selecione...</option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="GO">Goiás</option>
            <option value="ES">Espírito Santo</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraiba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí­</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SP">São Paulo</option>
            <option value="SC">Santa Catarina</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
          </select>
        </div>
        <div class="col-md-3">
          <label for="city" class="control-label">Cidade</label>
          <input type="text" class="form-control" id="city" placeholder="Digite sua cidade" />
        </div>
        <div class="col-md-3">
          <label for="district" class="control-label">Bairro</label>
          <input type="text" class="form-control" id="district" placeholder="Digite seu bairro" />
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-3">
          <label for="address" class="control-label">Endereço</label>
          <input type="text" class="form-control" id="address" placeholder="Digite seu endereço" />
        </div>
        <div class="col-md-3">
          <label for="complement" class="control-label">Complemento</label>
          <input type="text" class="form-control" id="complement" placeholder="Digite o complemento" />
        </div>
        <div class="col-md-3">
          <label for="number" class="control-label">Número</label>
          <input type="text" class="form-control" id="number" placeholder="Digite o número" />
        </div>
        <div class="col-md-3">
          <label for="address" class="control-label">CEP</label>
          <input type="text" class="form-control" id="address" placeholder="Digite seu CEP" />
        </div>
      </div>
      <div class="row form-group">
        <div class="col-md-4">
          <label for="email" class="control-label">E-mail</label>
          <input type="email" class="form-control" id="email" placeholder="Digite seu e-mail">
        </div>
        <div class="col-md-4">
          <label for="password" class="control-label">Senha</label>
          <input type="password" class="form-control" id="password" placeholder="Digite sua senha (mínimo 8 caracteres, contendo letras e símbolos)">
        </div>
        <div class="col-md-4">
          <label for="password" class="control-label">Confirmação de senha</label>
          <input type="password" class="form-control" id="password" placeholder="Digite novamente sua senha">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <div class="checkbox">
            <label>
              <input type="checkbox"> Desejo receber ofertas por e-mail
            </label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
          <button type="button" class="btn btn-default">Limpar</button>
        </div>
      </div>
    </fieldset>
  </form>
</div>
</apply>
