{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Handler.CartHandler
( addToCart
, removeFromCart
, checkoutCart
, updateCartItem
, viewCartItems
)
where

import Import
import Data.ByteString.Lazy (toStrict)
import qualified Data.Map as M
import Data.Text (Text)
--import qualified Data.Text as T
import qualified Data.Text.Encoding as T

--import DB.Postgresql.DBOrderItem as DBOrderItem
import DB.Postgresql.DBShirt as DBShirt
import GHC.Generics
import Handler.UtilHandler

type ItemId = Int64

data Item = Item
    { itemId       :: ItemId
    , itemName     :: Text
    , itemPrice    :: Float
    , itemQuantity :: Int
    }
  deriving (Eq, Generic, Show)


instance FromJSON Item
instance ToJSON Item


--data ItemReq = ItemReq
--    { itemReqId  :: Maybe ItemId
--    , itemReqQty :: Maybe Int
--    }
--  deriving (Eq, Generic, Show)

--instance FromJSON ItemReq
--instance ToJSON ItemReq


-- | We store only the id and quantities of items in session. A SessionItem
-- represents the quantity of a given item.
type SessionItem = Int
type Cart = M.Map ItemId SessionItem


cartKey :: Text
cartKey = "cart"


cartToStr :: Cart -> Text
cartToStr = T.decodeUtf8 . toStrict . encode . M.toList


initCart :: Handler App App ()
initCart = setCart M.empty


getCart :: Handler App App Cart
getCart = do
    maybeCartStr <- with sess $ getFromSession cartKey
    let maybeCart = maybeCartStr >>= decodeStrict . T.encodeUtf8
    maybe (initCart >> getCart) (return . M.fromList) maybeCart


setCart :: Cart -> Handler App App ()
setCart cart = withSession sess $
    with sess $ setInSession cartKey $ cartToStr cart
    

withCart :: (Cart -> Cart) -> AppHandler ()
withCart f = liftM f getCart >>= setCart


addToCart :: Handler App App ()
addToCart = do
    eitherItem <- fmap eitherDecode $ readRequestBody 2000 
    case eitherItem of
        Right (item :: M.Map Text Int64) -> do
            let (Just iid) = M.lookup "item" item
                (Just qty) = M.lookup "qty" item
            maybeShirt <- with db $ DBShirt.findShirtById iid
            if isNothing maybeShirt
                then notFound
                else do
                    cart <- M.insert iid (fromIntegral qty) <$> getCart
                    setCart cart
        _ -> badReq


updateCartItem :: Handler App App ()
updateCartItem = undefined


removeFromCart :: Int64 -> Handler App App ()
removeFromCart iid = withCart $ M.delete iid


viewCartItems :: Handler App App [Item]
viewCartItems = do
    cart <- getCart
    with db $ mapM sessionItemToItem $ M.toList cart
  where
    err1 sid = fail $ "Shirt with id " ++ show sid ++ " not found."
    sessionItemToItem (sid, qty) = do
        maybeShirt <- DBShirt.findShirtById sid
        case maybeShirt of
            Just s -> return
                Item { itemId       = fromJust $ shirtId s
                     , itemName     = shirtStamp s
                     , itemPrice    = shirtPrice s
                     , itemQuantity = qty
                     }
            _ -> err1 sid


checkoutCart :: Handler App App ()
checkoutCart = undefined
   
