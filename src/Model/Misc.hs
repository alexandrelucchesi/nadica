module Model.Misc where

-- | Typeclass for enumerable types.
class HasCode a where
    fromCode :: Int -> a
    getCode :: a -> Int


