{-# LANGUAGE OverloadedStrings #-}

------------------------------------------------------------------------------
-- | This module is where all the routes and handlers are defined for your
-- site. The 'app' function is the initializer that combines everything
-- together and is exported by this module.
module Site
  ( app
  ) where

------------------------------------------------------------------------------
import           Control.Applicative
import           Data.ByteString (ByteString)
import qualified Data.Text as T
import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.Auth
import           Snap.Snaplet.Auth.Backends.JsonFile
import           Snap.Snaplet.Heist
import           Snap.Snaplet.Session.Backends.CookieSession
import           Snap.Util.FileServe
import           Heist
import qualified Heist.Interpreted as I
------------------------------------------------------------------------------
import           Application

import qualified Text.XmlHtml as X
import           Data.Monoid
------------------------------------------------------------------------------
import           Control.Monad.IO.Class
import           Data.Aeson
import qualified DB.Postgresql.DBUser as DBUser
--import           Snap.Extras.CoreUtils
--import           Snap.Extras.JSON
import           Snap.Snaplet.PostgresqlSimple
import           Snap.Snaplet.Auth.Backends.PostgresqlSimple

import           Handler.CartHandler
import           Handler.StoreHandler
import           Handler.UtilHandler

------------------------------------------------------------------------------
-- | Handle login submit
handleLoginForm :: Handler App (AuthManager App) ()
handleLoginForm =
    loginUser "email" "password" (Just "remember")
              (\_ -> do
                modifyResponse (setResponseCode 403)
                modifyResponse (setContentType "text/plain;charset=utf-8")
                writeText err
              ) (return ())
  where
      err = "E-mail ou senha inválidos."


------------------------------------------------------------------------------
-- | Logs out and redirects the user to the site index.
handleLogout :: Handler App (AuthManager App) ()
handleLogout = logout >> redirect "/"


------------------------------------------------------------------------------
-- | Handle new user form submit
handleRegistrationForm :: Handler App (AuthManager App) ()
handleRegistrationForm = do --registerUser "login" "password" >> redirect "/"
    eitherUser <- fmap eitherDecode $ readRequestBody 2000 
    case eitherUser of
        Right user -> do
            _ <- withTop db $ DBUser.create user
            return ()
        Left err -> do
            liftIO $ print err
            modifyResponse $ setResponseStatus 400 "Bad request"
    

------------------------------------------------------------------------------
-- | The application's routes.
routes :: [(ByteString, Handler App App ())]
routes = [ ("/login",    with auth $ method POST handleLoginForm)
         , ("/logout",   with auth $ handleLogout)
         , ("/register", with auth $ method POST handleRegistrationForm)
         , ("/cart/viewCartItems", method GET viewCartItems >>= toJSONResp)
         , ("/cart/addToCart", method POST addToCart)
         , ("/cart/removeFromCart/:itemId",
                getInt64Param "itemId" >>= method DELETE . removeFromCart)
         , ("",          serveDirectory "static")
         , ("/store/listItems", method GET listItems >>= toJSONResp)
         ]


    
--debugReq :: Handler App App ()
--debugReq = do
--    rq <- getRequest
--    liftIO $ putStrLn "Rq cookies"
--    liftIO $ print $ rqCookies rq
--
--debugResp :: Handler App App ()
--debugResp = do
--    resp <- getResponse
--    liftIO $ putStrLn "Resp cookies"
--    liftIO $ print $ getResponseCookies resp

------------------------------------------------------------------------------
-- | The application initializer.
app :: SnapletInit App App
app = makeSnaplet "app" "An snaplet example application." Nothing $ do
    h <- nestSnaplet "" heist $ heistInit "templates"

    s <- nestSnaplet "sess" sess $
           initCookieSessionManager "site_key.txt" "sess" (Just 3600)
    d <- nestSnaplet "db" db pgsInit
    a <- nestSnaplet "auth" auth $ initPostgresAuth sess d

    addRoutes routes
    addAuthSplices h auth
--    wrapSite (\h -> debugReq >> h >> debugResp)
    return $ App h s d a

