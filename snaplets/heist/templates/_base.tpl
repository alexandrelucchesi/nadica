<!DOCTYPE html>
<html lang="pt-br" ng-app="nadicaApp">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>nadica</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="/css/master.css" rel="stylesheet">
    <custom-css />
  </head>
  <body ng-controller="CartCtrl">
    <!-- Workaround to not use "margin-top"/hide blank pixels at navbar corners. -->
    <div id="fill-black"></div>
    <!-- Navbar -->
    <div class="navbar-wrapper">
      <div class="container">
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="logo navbar-brand" href="/"><span class="sr-only">nadica</span></a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Loja <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Personagens</a></li>
                    <li><a href="#">Infantis</a></li>
                    <li><a href="#">Bandas</a></li>
                    <li><a href="#">Indianas</a></li>
                    <li><a href="#">Lisas</a></li>
                    <li class="divider"></li>
                    <!-- <li class="dropdown-header">Nav header</li>-->
                    <li><a href="#">Sugira um desenho</a></li>
                  </ul>
                </li>
                <li><a href="#about">Quem somos?</a></li>
                <li><a href="#contact">Contato</a></li>
                <li><a href="/faq">Dúvidas</a></li>
              </ul>
              <form id="search-id" class="navbar-form navbar-right" role="search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-search"></span></button>
                  </span>
                </div>
              </form>
              <ul class="nav navbar-nav navbar-right">
                <ifLoggedIn>
                  <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><loggedInUser /> <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="/account">Minha conta</a></li>
                        <li class="divider"></li>
                        <li><a href="/logout">Logout</a></li>
                      </ul>
                    </li>
                  </ul>
                </ifLoggedIn>
                <ifLoggedOut>
                  <li><a href="/login">Login</a></li>
                </ifLoggedOut>
                <li>
                  <a href="/cart" tooltip-html-unsafe="{{cartHtmlStr}}"
                      tooltip-placement="bottom" tooltip-append-to-body="true">
                      <span class="glyphicon glyphicon-shopping-cart"></span>
                          &nbsp;&nbsp;{{items.length}}
                          <span ng-show="items.length == 1">item</span>
                          <span ng-show="items.length != 1">itens</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <apply-content />

    <!-- FOOTER -->
    <div class="container">
      <hr class="footer-divider" />
      <footer>
        <p class="pull-right"><a href="#">^ Topo</a></p>
        <p>&copy; 2014 nadica, Inc. · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.js"></script>
    <script src="/js/docs.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/ui-utils.min.js"></script>
    <script src="/js/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="/js/app/controllers.js"></script>
    <custom-scripts />
  </body>
</html>
