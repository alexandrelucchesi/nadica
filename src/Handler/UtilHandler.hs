{-# LANGUAGE OverloadedStrings #-}

module Handler.UtilHandler where

import Import
import Data.ByteString (ByteString)
import Data.ByteString.Char8 as C
import Data.ByteString.Lazy (toStrict)
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T


-- -----------------------------------------------------------------------------
-- -- ** Utilitary functions.

-- -----------------------------------------------------------------------------
-- | HTTP responses.
badReq :: MonadSnap m => m a
badReq = finishWith (setResponseCode 400 emptyResponse)


notFound :: MonadSnap m => m ()
notFound = finishWith (setResponseCode 404 emptyResponse)


-- -----------------------------------------------------------------------------
-- | JSON handling.
parseJSONReq :: (MonadSnap m, FromJSON a) => m a
parseJSONReq = do
    eitherObj <- fmap eitherDecode $ readRequestBody 2000 
    either (const badReq) return eitherObj


toJSONResp :: (MonadSnap m, ToJSON a) => a -> m ()
toJSONResp obj = do
    modifyResponse $ setContentType "application/json"
    writeLBS $ encode obj


-- -----------------------------------------------------------------------------
-- | Snap params handling.
getInt64Param :: MonadSnap m => ByteString -> m Int64
getInt64Param name = do
    maybeParam <- getParam name
    case maybeParam of
        Just param -> maybe badReq (return . fromInteger . fst) $
            C.readInteger param
        _          -> badReq
            
