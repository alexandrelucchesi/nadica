{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DB.Postgresql.DBUser
    ( create
    , delete
    , findUserById
    , list
    , update
    ) where

import           Application
import           Control.Applicative                ((<$>))
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Int                           (Int64)
import           Data.Maybe                         (fromJust, isJust,
                                                     isNothing, listToMaybe)
import           Data.Text                          (Text)
import qualified Data.Text                          as T
import           Data.Text.Encoding                 as T (encodeUtf8)
import           Data.Text.Read                     (decimal)
import           Data.Time                          (getCurrentTime, utctDay)
import qualified Database.PostgreSQL.Simple.ToField as P
import           Database.PostgreSQL.Simple.Types   (Query (..))

import           Model.User
import           Snap.Snaplet
import qualified Snap.Snaplet.Auth                  as SA
import           Snap.Snaplet.PostgresqlSimple


-- -----------------------------------------------------------------------------
-- -- ** Data Types
-- | Datatype containing the names of the columns for the users table.
data UserTable =  UserTable
    {  userTblName   :: Text
    ,  colId         :: Text
    ,  colName       :: Text
    ,  colSurname    :: Text
    ,  colBirthday   :: Text
    ,  colGender     :: Text
    ,  colRg         :: Text
    ,  colCpf        :: Text
    ,  colPhone      :: Text
    ,  colCountry    :: Text
    ,  colState      :: Text
    ,  colCity       :: Text
    ,  colDistrict   :: Text
    ,  colAddress    :: Text
    ,  colComplement :: Text
    ,  colNumber     :: Text
    ,  colZipCode    :: Text
    }


defUserTable :: UserTable
defUserTable = UserTable
    {  userTblName   = "tb_user"
    ,  colId         = "user_id"
    ,  colName       = "name"
    ,  colSurname    = "surname"
    ,  colBirthday   = "birthday"
    ,  colGender     = "gender"
    ,  colRg         = "rg"
    ,  colCpf        = "cpf"
    ,  colPhone      = "phone"
    ,  colCountry    = "country"
    ,  colState      = "state"
    ,  colCity       = "city"
    ,  colDistrict   = "district"
    ,  colAddress    = "address"
    ,  colComplement = "complement"
    ,  colNumber     = "number"
    ,  colZipCode    = "zip_code"
    }


-- | List of deconstructors so it's easier to extract column names from an
-- 'UserTable'.
userColDef :: [(UserTable -> Text, User -> P.Action)]
userColDef =
    [ (colId        , P.toField . userId)
    , (colName      , P.toField . userName)
    , (colSurname   , P.toField . userSurname)
    , (colBirthday  , P.toField . userBirthday)
    , (colGender    , P.toField . userGender)
    , (colRg        , P.toField . userRg)
    , (colCpf       , P.toField . userCpf)
    , (colPhone     , P.toField . userPhone)
    , (colCountry   , P.toField . userCountry)
    , (colState     , P.toField . userState)
    , (colCity      , P.toField . userCity)
    , (colDistrict  , P.toField . userDistrict)
    , (colAddress   , P.toField . userAddress)
    , (colComplement, P.toField . userComplement)
    , (colNumber    , P.toField . userNumber)
    , (colZipCode   , P.toField . userZipCode)
    ]


-- | Datatype containing the names of the columns for the 'snap_auth_user' table.
data AuthTable
    =  AuthTable
    {  authTblName :: Text
    ,  colUid      :: Text
    ,  colLogin    :: Text
    ,  colEmail    :: Text
    ,  colPassword :: Text
    }


defAuthTable :: AuthTable
defAuthTable = AuthTable
    {  authTblName = "snap_auth_user"
    ,  colUid      = "uid"
    ,  colLogin    = "login"
    ,  colEmail    = "email"
    ,  colPassword = "password"
    }


-- | List of deconstructors so it's easier to extract column names from an
-- 'AuthTable'.
authTableColDef :: [(AuthTable -> Text, User -> P.Action)]
authTableColDef =
  [ (colUid     , P.toField . userId)
  , (colLogin   , P.toField . userEmail)
  , (colEmail   , P.toField . userEmail)
  , (colPassword, P.toField . userPassword)
  ]


------------------------------------------------------------------------------
-- | Util
authUserId :: SA.AuthUser -> Maybe Int64
authUserId authUser = do
  maybeUserId <- SA.unUid <$> SA.userId authUser
  let eitherUserId = decimal maybeUserId
  either (const Nothing) (return . fst) eitherUserId


------------------------------------------------------------------------------
-- | CRUD

-- | Builds the 'create query'. When inserting an user, the user id which
-- references the 'snap_auth_user' table MUST be specified.
createQuery :: UserTable -> User -> (Text, [P.Action])
createQuery ut u@User{..} =
    (T.concat [ "INSERT INTO "
              , userTblName ut
              , " ("
              , T.intercalate "," cols
              , ") VALUES ("
              , T.intercalate "," vals
              , ") RETURNING "
              , T.intercalate "," cols
              ]
              , params)
  where
    cols = map (($ut) . fst) userColDef
    vals = map (const "?") cols
    params = map (($u) . snd) userColDef


-- | Creates a new user. Field 'userId' must be 'Nothing'.
create :: User -> Handler App Postgres (Either ErrorList User)
create user = do
    unless (isNothing $ userId user) err1
    today <- liftIO $ fmap (Date . utctDay) getCurrentTime
    case validateUser today user of
        -- Any exception within the context of 'withTransaction' will cause a rollback.
        [] -> withTransaction $ do
            authUser <- withTop auth insertAuthTable
            newUser <- insertUserTable $ fromJust $ authUserId authUser
            return $ Right newUser { userEmail = SA.userLogin authUser }
        xs -> do
            return $ Left xs
  where
    err1 = fail "Can't create user with specified 'id'."

    insertAuthTable :: Handler App (SA.AuthManager App) SA.AuthUser
    insertAuthTable = do
        password <- liftIO $ SA.encryptPassword . SA.ClearText . T.encodeUtf8 $
            userPassword user
        now <- liftIO getCurrentTime
        let newAuthUser = SA.defAuthUser
                { SA.userLogin     = userEmail user
                , SA.userPassword  = Just password
                , SA.userEmail     = Just $ userEmail user
                , SA.userCreatedAt = Just now
                , SA.userUpdatedAt = Just now
                }
        eitherAuthUser <- SA.saveUser newAuthUser
        either (error . show) return eitherAuthUser

    insertUserTable :: Int64 -> Handler App Postgres User
    insertUserTable newUserId = do
      let newUser = user { userId = Just newUserId }
          (qstr, params) = createQuery defUserTable newUser
          q = Query $ T.encodeUtf8 qstr
      ([u] :: [User]) <- query q params
      return u


-- | Builds the update query.
updateQuery :: UserTable -> User -> (Text, [P.Action])
updateQuery ut u@User{..} =
    (T.concat [ "UPDATE "
              , userTblName ut
              , " SET "
              , T.intercalate "," (map (qval . fst) $ tail userColDef)
              , " WHERE "
              , colId ut
              , " = ? RETURNING "
              , T.intercalate "," (map (($ut) . fst) userColDef)
              ]
    , params ++ [P.toField userId])
  where
    qval f = (f ut) `T.append` " = ?"
    params = map (($u) . snd) $ tail userColDef

-- | Updates an existing user. Field 'userId' MUST be specified (that is, be 'Just' something).
update :: User -> Handler App Postgres (Either ErrorList User)
update user = do
    today <- liftIO $ fmap (Date . utctDay) getCurrentTime
    unless (isJust $ userId user) err1
    case validateUser today user of
        -- Any exception within the context of 'withTransaction' will cause a rollback.
        [] -> withTransaction $ do
            authUser <- withTop auth updateAuthTable
            user' <- updateUserTable
            return $ Right user' { userEmail = SA.userLogin authUser }
        xs -> do
            return $ Left xs
  where
    err1 = fail "Can't update user with no 'id'."
    err2 = fail "Specified user doesn't exist."

    updateAuthTable :: Handler App (SA.AuthManager App) SA.AuthUser
    updateAuthTable = do
        maybeAuthUser <- SA.withBackend (\b -> liftIO $
            SA.lookupByUserId b $ SA.UserId . T.pack . show . fromJust $ userId user)
        authUser <- maybe err2 return maybeAuthUser
        password <- liftIO $ SA.encryptPassword . SA.ClearText . T.encodeUtf8 $
            userPassword user
        now <- liftIO getCurrentTime
        let newAuthUser = authUser
                { SA.userLogin     = userEmail user
                , SA.userPassword  = Just password
                , SA.userEmail     = Just $ userEmail user
                , SA.userUpdatedAt = Just now
                }
        eitherAuthUser <- SA.saveUser newAuthUser
        either (error . show) return eitherAuthUser

    updateUserTable :: Handler App Postgres User
    updateUserTable = do
      let (qstr, params) = updateQuery defUserTable user
          q = Query $ T.encodeUtf8 qstr
      ([u] :: [User]) <- query q params -- Should return a singleton list.
      return u


-- | Builds 'find queries'. If the 3rd parameter is 'Nothing', returns a 'select
-- all' query.
findQuery :: UserTable -> AuthTable -> Maybe Int64 -> (Text, [P.Action])
findQuery UserTable{..} AuthTable{..} maybeUserId =
    case maybeUserId of
        Just userId ->
            (T.concat [ selectQuery
                      , " WHERE "
                      , colId, " = ?"
                      ]
            , [P.toField userId])
        _ -> (selectQuery, [])
  where
    selectQuery =
        T.concat [ "SELECT "
                 , userTblName, ".*, "
                 , authTblName, ".", colLogin
                 , ", "
                 , authTblName, ".", colPassword
                 , " FROM "
                 , userTblName
                 , " INNER JOIN "
                 , authTblName
                 , " ON "
                 , colId, " = ", colUid
                 ]


-- | Lists all users.
list :: Handler App Postgres [User]
list = do
    let (qstr, _) = findQuery defUserTable defAuthTable Nothing
        q = Query $ T.encodeUtf8 qstr
    query_ q


findUserById :: Int64 -> Handler App Postgres (Maybe User)
findUserById userId = do
    let (qstr, params) = findQuery defUserTable defAuthTable (Just userId)
        q = Query $ T.encodeUtf8 qstr
    us <- query q params
    unless (length us <= 1) err1
    return $ listToMaybe us
  where
    err1 = fail $ "It shouldn't happen: 'findUserById' returned more than one user."


-- | Builds 'delete queries'. If the 3rd parameter is 'Nothing', returns a
-- 'delete all' query.
deleteQuery :: UserTable -> Int64 -> (Text, [P.Action])
deleteQuery UserTable{..} userId =
    (T.concat [ "DELETE FROM "
              , userTblName
              , " WHERE "
              , colId, " = ?"]
    , [P.toField userId])


-- | Deletes an user by id.
delete :: Int64 -> Handler App Postgres ()
delete userId = do
    maybeAuthUser <- withTop auth $ SA.withBackend (\b -> liftIO $
        SA.lookupByUserId b $ SA.UserId . T.pack $ show userId)
    authUser <- maybe err1 return maybeAuthUser
    withTransaction $ do
        deleteUserTable
        withTop auth $ deleteAuthTable authUser
  where
    err1 = fail $ "User with 'id': " ++ show userId ++ " was not found."

    deleteUserTable :: Handler App Postgres ()
    deleteUserTable = do
      let (qstr, params) = deleteQuery defUserTable userId
          q = Query $ T.encodeUtf8 qstr
      void $ execute q params

    deleteAuthTable :: SA.AuthUser -> Handler App (SA.AuthManager App) ()
    deleteAuthTable = SA.destroyUser

