# Installation

Requires PostgreSQL to be installed prior to running `cabal install`. On Ubuntu:

    * #> apt-get install postgresql libpq-dev

Then you should create the database, etc, according to the information in the
file `snaplets/postgresql-simple/devel.cfg`.

