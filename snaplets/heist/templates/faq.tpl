<apply template="_base">
<div class="container">
  <div class="page-header">
    <h1>Perguntas Frequentes (FAQ)</h1>
  </div>
  <ol>
      <li><a href="#q1">Como funciona o processo de sugerir um desenho?</a></li>
      <li><a href="#q2">É importante eu me cadastrar para comprar? Por que eu deveria?</a></li>
      <li><a href="#q3">Como se da o funcionamento do frete?</a></li>
      <li><a href="#q4">Qual o prazo de entrega do produto?</a></li>
      <li><a href="#q5">Quais são as formas de pagamento?</a></li>
      <li><a href="#q6">A forma de pagamento influencia no prazo de entrega?</a></li>
      <li><a href="#q7">Como faço para verificar o andamento do meu pedido e a validação de uma compra?</a></li>
  </ol>
  
  <div id="q1">
      <h2>Como funciona o processo de sugerir um desenho?</h2>
      <p>O sugira seu desenho é um método de interação entre os clientes, onde qualquer um poderá nos enviar desenhos que serão escolhidos e postados para a avaliação. O desenho que tiver maior quantidade de avaliações positivas ("likes"), será produzido e estará disponível para vendas no site.</p>
  </div>
  
  <div id="q2">
      <h2>É importante eu me cadastrar para comprar? Por que eu deveria?</h2>
      <p>Sim, pois desta forma será possível logar no site e acompanhar o status de todos seus pedidos desde a compra até o envio, além de poder receber ofertas exclusivas e estar sempre ligado nos lançamentos do nadica.</p>
  </div>
  
  <div id="q3">
      <h2>Como se da o funcionamento do frete?</h2>
      <p>O frete será calculado na hora da compra a partir do CEP fornecido pelo cliente, vale ressaltar que o frete será agregado ao valor do produto.</p>
  </div>
  
  <div id="q4">
      <h2>Qual o prazo de entrega do produto?</h2>
      <p>Sua compra será processada e enviada em até dois dias úteis após a data de confirmação do pagamento. Já o prazo de entrega varia de acordo com o local destinado e a opção de frete selecionada no momento da compra.</p>
  </div>
  
  <div id="q5">
      <h2>Quais são as formas de pagamento?</h2>
      <p>Será realizado pelo PagSeguro, podendo pagar através de cartões de crédito, boleto, débito online e depósito em conta. Mais detalhes em https://pagseguro.uol.com.br/</p>
  </div>
  
  <div id="q6">
      <h2>A forma de pagamento influencia no prazo de entrega?</h2>
      <p>Sim, pois certas formas de pagamento demoram para serem efetivadas, influenciando assim a validação da compra e por consequência a data do envio.</p>
  </div>
  
  <div id="q7">
      <h2>Como faço para verificar o andamento do meu pedido e a validação de uma compra?</h2>
      <p>Acompanhe sua compra clicando em "Meus pedidos" na parte superior do site caso seja cadastrado e esteja logado. Você receberá um email de confirmação do pagamento..</p>
  </div>
</div>
</apply>
