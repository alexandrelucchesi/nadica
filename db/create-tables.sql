-- Clear database
DROP TABLE IF EXISTS tb_order_item;
DROP TABLE IF EXISTS tb_order;
DROP TABLE IF EXISTS tb_shirt;
DROP TABLE IF EXISTS tb_user;
-- End Clear

CREATE TABLE tb_user (
    user_id    serial NOT NULL,
    name       text NOT NULL,
    surname    text NOT NULL,
    birthday   date NOT NULL,
    gender     smallint NOT NULL, -- 0|1|2|9 (ISO ISO/IEC 5218).
    rg         text NOT NULL,
    cpf        text NOT NULL,
    phone      varchar(11) NOT NULL, -- SP mobile numbers are 9 digits. 
    country    text NOT NULL,
    state      text NOT NULL,
    city       text NOT NULL,
    district   text NOT NULL,
    address    text NOT NULL,
    complement text NOT NULL DEFAULT '',
    number     text NOT NULL,
    zip_code   text NOT NULL,

    PRIMARY KEY (user_id),
    CONSTRAINT tb_user_uid_fkey FOREIGN KEY (user_id)
        REFERENCES snap_auth_user (uid) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE tb_shirt (
    shirt_id    serial PRIMARY KEY,
    stamp       text NOT NULL UNIQUE, -- Estampa da camisa.
    description text NOT NULL DEFAULT '',
    color       text NOT NULL,
    price       numeric(5,2) NOT NULL, -- Max. price set to R$ 999,99.
    inventory   smallint NOT NULL
);

INSERT INTO tb_shirt (stamp, color, price, inventory)
    VALUES ('Capitão América', 'Branca', 44.90, 10);

INSERT INTO tb_shirt (stamp, color, price, inventory)
    VALUES ('Hulk', 'Branca', 44.90, 5);

INSERT INTO tb_shirt (stamp, color, price, inventory)
    VALUES ('Batman', 'Branca', 44.90, 3);


CREATE TABLE tb_order (
    order_id   serial PRIMARY KEY,
    user_id    integer NOT NULL,
    created_at timestamptz NOT NULL,
    status     smallint NOT NULL, -- TODO: Define status codes.

    CONSTRAINT tb_order_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES tb_user (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE tb_order_item (
    order_id   integer NOT NULL,
    shirt_id   integer NOT NULL,
    size_style  text NOT NULL, -- Eg.: P - Baby Look (P-BL).
    quantity   int NOT NULL,

    PRIMARY KEY (order_id, shirt_id),
    CONSTRAINT tb_order_item_order_id_fkey FOREIGN KEY (order_id)
        REFERENCES tb_order (order_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT tb_order_item_shirt_id_fkey FOREIGN KEY (shirt_id)
        REFERENCES tb_shirt (shirt_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

