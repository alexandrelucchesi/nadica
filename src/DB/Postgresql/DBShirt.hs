{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DB.Postgresql.DBShirt
    ( findShirtById
    , listShirts
    , listShirtsByOrderId
    , module Model.Shirt
    ) where

import           Application
import           Control.Applicative                ((<$>))
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Int                           (Int64)
import           Data.Maybe                         (catMaybes, fromJust, isJust,
                                                     isNothing, listToMaybe)
import           Data.Text                          (Text)
import qualified Data.Text                          as T
import           Data.Text.Encoding                 as T (encodeUtf8)
import           Data.Text.Read                     (decimal)
import           Data.Time                          (getCurrentTime, utctDay)
import qualified Database.PostgreSQL.Simple.ToField as P
import           Database.PostgreSQL.Simple.Types   (Query (..))

import           Model.Shirt
import           Snap.Snaplet
import qualified Snap.Snaplet.Auth                  as SA
import           Snap.Snaplet.PostgresqlSimple
import qualified DB.Postgresql.DBOrderItem          as DBOrderItem


-- -----------------------------------------------------------------------------
-- -- ** Data Types
-- | Datatype containing the names of the columns for the users table.
data ShirtTable =  ShirtTable
    { shirtTblName   :: Text
    , colShirtId     :: Text
    , colStamp       :: Text
    , colDescription :: Text
    , colColor       :: Text
    , colPrice       :: Text
    , colInventory   :: Text
    }


defShirtTable :: ShirtTable
defShirtTable = ShirtTable
    {  shirtTblName   = "tb_shirt"
    ,  colShirtId     = "shirt_id"
    ,  colStamp       = "stamp"
    ,  colDescription = "description"
    ,  colColor       = "color"
    ,  colPrice       = "price"
    ,  colInventory   = "inventory"
    }


-- | List of deconstructors so it's easier to extract column names from an
-- 'UserTable'.
shirtColDef :: [(ShirtTable -> Text, Shirt -> P.Action)]
shirtColDef =
    [ (colShirtId     , P.toField . shirtId)
    , (colStamp       , P.toField . shirtStamp)
    , (colDescription , P.toField . shirtDescription)
    , (colColor       , P.toField . shirtColor)
    , (colPrice       , P.toField . shirtPrice)
    , (colInventory   , P.toField . shirtInventory)
    ]

------------------------------------------------------------------------------
-- | CRUD

-- | Builds 'find queries'. If the 3rd parameter is 'Nothing', returns a 'select
-- all' query.
findQuery :: ShirtTable -> Maybe Int64 -> (Text, [P.Action])
findQuery ShirtTable{..} maybeShirtId =
    case maybeShirtId of
        Just shirtId ->
            (T.concat [ selectQuery
                      , " WHERE "
                      , colShirtId, " = ?"
                      ]
            , [P.toField shirtId])
        _ -> (selectQuery, [])
  where
    selectQuery =
        T.concat [ "SELECT * FROM "
                 , shirtTblName
                 ]

-- | Lists all users.
listShirts :: Handler App Postgres [Shirt]
listShirts = do
    let (qstr, _) = findQuery defShirtTable Nothing
        q = Query $ T.encodeUtf8 qstr
    query_ q


findShirtById :: Int64 -> Handler App Postgres (Maybe Shirt)
findShirtById sid = do
    let (qstr, params) = findQuery defShirtTable (Just sid)
        q = Query $ T.encodeUtf8 qstr
    ss <- query q params
    unless (length ss <= 1) err1
    return $ listToMaybe ss
  where
    err1 = fail $ "It shouldn't happen: 'findShirtById' returned more than one shirt."


listShirtsByOrderId :: Int64 -> Handler App Postgres [Shirt]
listShirtsByOrderId oid = undefined--do
--    let (qstr, params) = DBOrderItem.findQuery oid
--        q = Query $ T.encodeUtf8 qstr
--    (oes :: [(Int64,Int64,Int)]) <- query q params
--    let shirtsIds = map (\(_,sid,_) -> sid) oes
--    catMaybes <$> mapM findShirtById shirtsIds
    

