{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TestShirt where

import           Application
import           Control.Exception             (SomeException)
import           Control.Applicative
import           Control.Monad                 (void)
import qualified Control.Monad.CatchIO         as E
import           Control.Monad.IO.Class        (liftIO)
import           Data.Int                      (Int64)
import qualified Data.Map                      as M
import           Data.Text                     (Text)
import           Data.Time.Calendar
import           Data.Typeable
import qualified Data.Map                      as M
import           DB.Postgresql.DBShirt
import           Model.Shirt
import           Site                          (app)
import           Snap.Snaplet
import qualified Snap.Snaplet.Auth             as SA
import           Snap.Snaplet.PostgresqlSimple
import           Snap.Snaplet.Test
import qualified Snap.Test                     as ST


testListShirts :: IO (Either Text [Shirt])
testListShirts =
    evalHandler Nothing (ST.get "" M.empty) (with db listShirts) app

testFindShirtById :: Int64 -> IO (Either Text (Maybe Shirt))
testFindShirtById sid =
    evalHandler Nothing (ST.get "" M.empty) (with db $ findShirtById sid) app

testListShirtsByOrderId :: Int64 -> IO (Either Text [Shirt])
testListShirtsByOrderId oid =
    evalHandler Nothing (ST.get "" M.empty) (with db $ listShirtsByOrderId oid) app

