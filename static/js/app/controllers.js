var nadicaApp = angular.module('nadicaApp', ['ui.mask', 'ui.bootstrap']);

nadicaApp.controller('LoginCtrl', function ($scope, $http) {
    $scope.email = '';
    $scope.password = '';
    $scope.remember = false;
    $scope.submitted = false;
    $scope.rspMsg = '';
    $scope.ok = null;
  
    $scope.login = function (isValid) {
        $scope.submitted = true;
        if (isValid) {
            $http({
                method  : 'POST',
                url     : '/login',
                data    : $.param({
                  email    : $scope.email,
                  password : $scope.password,
                  remember : $scope.remember ? "1" : "0"
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function () {
                $scope.ok = true;
                $scope.rspMsg = "Login efetuado com sucesso! Redirecionando...";
                setTimeout(function(){
                  window.location.href = '/';
                }, 2000);
            }).error(function (data) {
                $scope.ok = false;
                $scope.rspMsg = data;
            });
        }
    };

});

nadicaApp.controller('RegisterCtrl', function ($scope, $http) {
    var defUser =
        { userName       : ''
        , userSurname    : ''
        , userBirthday   : ''
        , userGender     : ''
        , userRg         : ''
        , userCpf        : ''
        , userPhone      : ''
        , userCountry    : 'Brasil'
        , userState      : ''
        , userCity       : ''
        , userDistrict   : ''
        , userAddress    : ''
        , userComplement : ''
        , userNumber     : ''
        , userZipCode    : ''
        , userEmail      : ''
        , userPassword   : ''
        }
  
    var testUser = 
        { userAddress              : "QSF 13"
        , userBirthday             : "19091991"
        , userCity                 : "Brasília"
        , userComplement           : "casa"
        , userCountry              : "Brasil"
        , userCpf                  : "03689326133"
        , userDistrict             : "Taguatinga Sul"
        , userEmail                : "joao@brubles.com"
        , userGender               : 1
        , userName                 : "João"
        , userNumber               : "101"
        , userPassword             : "190991"
        , userPasswordConfirmation : "190991"
        , userPhone                : "6182079397"
        , userRg                   : "2635878"
        , userState                : "DF"
        , userSurname              : "Lucchesi Alencar"
        , userZipCode              : "72025630"
        };

    $scope.user = defUser;
    //$scope.user = testUser;

    $scope.states =
        [ { code : ''  , state : 'Selecione...' }
        , { code : 'AC', state : 'Acre' }
        , { code : 'AL', state : 'Alagoas' }
        , { code : 'AP', state : 'Amapá' }
        , { code : 'AM', state : 'Amazonas' }
        , { code : 'BA', state : 'Bahia' }
        , { code : 'CE', state : 'Ceará' }
        , { code : 'DF', state : 'Distrito Federal' }
        , { code : 'GO', state : 'Goiás' }
        , { code : 'ES', state : 'Espírito Santo' }
        , { code : 'MA', state : 'Maranhão' }
        , { code : 'MT', state : 'Mato Grosso' }
        , { code : 'MS', state : 'Mato Grosso do Sul' }
        , { code : 'MG', state : 'Minas Gerais' }
        , { code : 'PA', state : 'Pará' }
        , { code : 'PB', state : 'Paraiba' }
        , { code : 'PR', state : 'Paraná' }
        , { code : 'PE', state : 'Pernambuco' }
        , { code : 'PI', state : 'Piauí' }
        , { code : 'RJ', state : 'Rio de Janeiro' }
        , { code : 'RN', state : 'Rio Grande do Norte' }
        , { code : 'RS', state : 'Rio Grande do Sul' }
        , { code : 'RO', state : 'Rondônia' }
        , { code : 'RR', state : 'Roraima' }
        , { code : 'SP', state : 'São Paulo' }
        , { code : 'SC', state : 'Santa Catarina' }
        , { code : 'SE', state : 'Sergipe' }
        , { code : 'TO', state : 'Tocantins' }
        ];

    $scope.genders =
        [ { code : '', gender : 'Selecione...' }
        , { code : 1 , gender : 'Masculino' }
        , { code : 2 , gender : 'Feminino' }
        ];

    $scope.submitted = false;
    $scope.register = function (isValid) {
        $scope.submitted = true;
        // TODO: Validations!
        if ($scope.user.userPhone.slice(0,3) == '119'
            && $scope.user.userPhone.length != 9) { // São Paulo mobile phones...
          // Error!
        }
  
        if (isValid) {
            /* Format birthday. */
            var birthday = $scope.user.userBirthday;
            var day = birthday.substring(0, 2);
            var month = birthday.substring(2, 4);
            var year = birthday.substring(4, 8);

            $scope.user.userBirthday = [year, month, day].join("-");

            $http.post('/register', $scope.user)
              .success(function () {
              }).error(function () {
              });
        }
  
        console.log($scope.user);
    };

});


nadicaApp.controller('CartCtrl', function ($scope, $http) {

    $scope.items = [];

    $scope.addToCart = function () {
        if ($scope.item && $scope.item.item && $scope.item.qty) {
            console.log($scope.item);
            $http.post('/cart/addToCart', $scope.item)
                .success(function () {
                    console.log('addToCart: It should have worked.');
                    $scope.refresh();
                }).error(function () {
                    console.log('addToCart: Some shit occurred.');
                });
        }
    };

    $scope.removeFromCart = function (itemId) {
        $http.delete('/cart/removeFromCart/' + itemId)
            .success(function () {
                console.log('removeFromCart: It should have worked.');
                $scope.refresh();
            }).error(function () {
                console.log('removeFromCart: Some shit occurred.');
            });
    };


    $scope.cartHtmlStr = '';

    $scope.$watch("items", function () {
        var cartItemsToHtml = function () {
            var htmlStr = "";
            if ($scope.items.length == 0) {
                htmlStr = "Seu carrinho está vazio!"
            } else {
                $scope.items.forEach(function (item) {
                    htmlStr += "<p class=\"text-left\">" + item.itemName
                    + " (" + item.itemQuantity + "x)" + "</p>";
                });
            }
            return htmlStr;
        };
        $scope.cartHtmlStr = cartItemsToHtml();
    });


    $scope.refresh = function () {
        $http.get('/cart/viewCartItems')
            .success(function (data) {
                $scope.items = data;
            }).error(function () {
                console.log('Some shit occurred.');
            });
    };


    $scope.refresh();

});


nadicaApp.controller('StoreCtrl', function ($scope, $http) {

    $scope.items = [];

    // Pagination related stuff
    $scope.pagStart = 0;
    $scope.qty = 12;

    $scope.nextPage = function (curPage) {
        $scope.pagStart = (curPage - 1) * $scope.qty;

        $http.get('/store/listItems?start=' + $scope.pagStart
                + '&qty=' + $scope.qty)
            .success(function (data) {
                $scope.items = data;
            }).error(function () {
                console.log('Some shit occurred.');
            });
    };

    $scope.prevPage = function () {

    };

});



