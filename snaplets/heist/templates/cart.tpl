<apply template="_base">
<div class="container">

  <div class="page-header">
    <h1>Meu Carrinho</h1>
  </div>

  <div class="row">
    <form name="addToCartForm" class="form-inline" role="form"
        ng-submit="addToCart()" novalidate>
      <fieldset class="col-md-12">
        <legend>Adicionar item</legend>
        <div class="form-group">
          <input id="item" name="item" type="number" class="form-control"
              ng-model="item.item" placeholder="Item ID" required />
        </div>
        <div class="form-group">
          <input id="qty" name="qty" type="number" class="form-control"
              ng-model="item.qty" placeholder="Quantidade" required />
        </div>
        <button type="submit" class="btn btn-primary">Adicionar</button>
        <button type="button" class="btn btn-default" ng-click="refresh()">Atualizar</button>
      </fieldset>
    </form>
  </div>

  <br />

  <div class="table-responsive">
    <table class="table table-striped table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Camiseta</th>
          <th>Valor Unitário</th>
          <th>Quantidade</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="i in items">
          <td>{{i.itemId}}</td>
          <td>{{i.itemName}}</td>
          <td>{{i.itemPrice | currency:"R$ "}}</td>
          <td>{{i.itemQuantity}}</td>
          <td>
            <button ng-click="removeFromCart(i.itemId)" class="btn btn-primary
                btn-sm">
              <span class="glyphicon glyphicon-edit"></span> Editar
              <span class="sr-only">Editar</span>
            </button>
            <button ng-click="removeFromCart(i.itemId)" class="btn btn-danger
                btn-sm">
              <span class="glyphicon glyphicon-remove"></span> Remover
              <span class="sr-only">Remover</span>
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

</div>
</apply>

