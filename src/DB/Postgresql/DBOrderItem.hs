{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DB.Postgresql.DBOrderItem
    ( createOrderItem
    , listOrderItemsByOrderId
    ) where

import           Import

import           Data.Text          (Text)
import qualified Data.Text          as T
import qualified Data.Text.Encoding as T
import           Model.OrderItem

-- -----------------------------------------------------------------------------
-- -- ** Data Types

-- -----------------------------------------------------------------------------
-- | Datatype containing the names of the columns for the table 'tb_order_item'.
data OrderItemTable = OrderItemTable
    { orderItemTblName      :: Text
    , colOrderItemOrderId   :: Text
    , colOrderItemShirtId   :: Text
    , colOrderItemSizeStyle :: Text
    , colOrderItemQuantity  :: Text
    }


defOrderItemTable :: OrderItemTable
defOrderItemTable = OrderItemTable
    { orderItemTblName      = "tb_order_entry"
    , colOrderItemOrderId   = "order_id"
    , colOrderItemShirtId   = "shirt_id"
    , colOrderItemSizeStyle = "size_style"
    , colOrderItemQuantity  = "quantity"
    }


-- -----------------------------------------------------------------------------
-- | List of deconstructors so it's easier to extract column names from an
-- 'OrderItemTable'.
orderItemColDef :: [(OrderItemTable -> Text, OrderItem -> Action)]
orderItemColDef =
    [ (colOrderItemOrderId   , toField . orderItemOrderId)
    , (colOrderItemShirtId   , toField . orderItemShirtId)
    , (colOrderItemSizeStyle , toField . orderItemSizeStyle)
    , (colOrderItemQuantity  , toField . orderItemQuantity)
    ]


-- -----------------------------------------------------------------------------
-- -- ** CRUD

-- -----------------------------------------------------------------------------
-- | Create.
createOrderItem :: OrderItem -> Handler App Postgres (Maybe OrderItem)
createOrderItem oi = do
    let (qstr, params) = createQuery oi
        q = Query $ T.encodeUtf8 qstr
    listToMaybe <$> query q params
  where
    createQuery :: OrderItem -> (Text, [Action])
    createQuery oi@OrderItem{..} =
        (T.concat [ "INSERT INTO "
                  , orderItemTblName ot
                  , " ("
                  , T.intercalate "," cols
                  , ") VALUES ("
                  , T.intercalate "," vals
                  , ") RETURNING "
                  , T.intercalate "," cols
                  ]
                  , params)
      where
        ot = defOrderItemTable
        cols = map (($ot) . fst) orderItemColDef
        vals = map (const "?") cols
        params = map (($oi) . snd) orderItemColDef


-- -----------------------------------------------------------------------------
-- | List.
listOrderItemsByOrderId :: Int64 -> Handler App Postgres [OrderItem]
listOrderItemsByOrderId oid = do
    let (qstr, _) = listOrderItemsByOrderIdQuery oid
        q = Query $ T.encodeUtf8 qstr
    query_ q
  where
    listOrderItemsByOrderIdQuery :: Int64 -> (Text, [Action])
    listOrderItemsByOrderIdQuery oiid =
        let OrderItemTable{..} = defOrderItemTable
        in (T.concat [ "SELECT * FROM "
                     , orderItemTblName
                     , " WHERE "
                     , colOrderItemOrderId, " = ?"
                     ]
        , [toField oiid])

