{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TestUser where

import           Application
import           Control.Exception             (SomeException)
import           Control.Applicative
import           Control.Monad                 (void)
import qualified Control.Monad.CatchIO         as E
import           Control.Monad.IO.Class        (liftIO)
import           Data.Int                      (Int64)
import qualified Data.Map                      as M
import           Data.Text                     (Text)
import           Data.Time.Calendar
import           Data.Typeable
import qualified Data.Map                      as M
import           DB.Postgresql.DBUser
import           Model.User                    as User
import           Site                          (app)
import           Snap.Snaplet
import qualified Snap.Snaplet.Auth             as SA
import           Snap.Snaplet.PostgresqlSimple
import           Snap.Snaplet.Test
import qualified Snap.Test                     as ST


testList :: IO (Either Text [User])
testList =
    evalHandler Nothing (ST.get "" M.empty) (with db list) app


testFindById :: Int64 -> IO (Either Text (Maybe User))
testFindById userId =
    evalHandler Nothing (ST.get "" M.empty) (with db $ findById userId) app


testDelete :: Int64 -> IO (Either Text ())
testDelete userId =
    evalHandler Nothing (ST.get "" M.empty) (with db $ delete userId) app


testCreate :: User -> IO (Either Text (Either ErrorList User))
testCreate user =
    evalHandler Nothing (ST.get "" M.empty) (with db $ create user) app


testUpdate :: User -> IO (Either Text (Either ErrorList User))
testUpdate user =
    evalHandler Nothing (ST.get "" M.empty) (with db $ update user) app


testCreate' :: Maybe Int64 -> User -> IO (Either Text (Either ErrorList User))
testCreate' uid user =
    evalHandler Nothing (ST.get "" M.empty) (with db $ create user { userId = uid }) app


testUpdate' :: Maybe Int64 -> User -> IO (Either Text (Either ErrorList User))
testUpdate' uid user =
    evalHandler Nothing (ST.get "" M.empty) (with db $ update user { userId = uid }) app


user1 :: User
user1 = User
    { userId        = Just 7
    , userName       = "Alexandre"
    , userSurname    = "Lucchesi Alencar"
    , userBirthday   = Date $ fromGregorian 1991 09 19
    , userGender     = female
    , userRg         = "2635878"
    , userCpf        = "03689326133"
    , userPhone      = "6182079397"
    , userCountry    = "Brasil"
    , userState      = "DF"
    , userCity       = "Brasília"
    , userDistrict   = "Taguatinga Sul"
    , userAddress    = "QSF 13"
    , userComplement = "casa"
    , userNumber     = "101"
    , userZipCode    = "72025630"
    , userEmail      = "alexandrelucchesi@gmail.com"
    , userPassword   = "Lucch3s1!01"
    }

user2 :: User
user2 = User
    { userId        = Just 7
    , userName       = "Test"
    , userSurname    = "Testing"
    , userBirthday   = Date $ fromGregorian 1991 09 19
    , userGender     = female
    , userRg         = "2635878"
    , userCpf        = "03689326133"
    , userPhone      = "6182079397"
    , userCountry    = "Brasil"
    , userState      = "DF"
    , userCity       = "Brasília"
    , userDistrict   = "Taguatinga Sul"
    , userAddress    = "QSF 13"
    , userComplement = ""
    , userNumber     = "101"
    , userZipCode    = "72025630"
    , userEmail      = "test@testing.com"
    , userPassword   = "Lucch3s1!01"
    }


-- ** Util

--cleanDatabase ::
cleanDatabase :: IO (Either Text ())
cleanDatabase =
    evalHandler Nothing
                (ST.get "" M.empty)
                (with db $ void $ do
                    _ <- execute_ "DELETE FROM tb_user"
                    execute_ "DELETE FROM snap_auth_user"
                )
                app


-- ** Tests
testFailing :: IO (Either Text ())
testFailing =
    evalHandler Nothing
                (ST.get "" M.empty)
                (handlerCatchesException $ do
                    callingFail
                    return ()
                    patternMatchingError
                    callingEmpty
                    callingError
                )
                app
  where
    handlerCatchesException :: E.MonadCatchIO m => m () -> m ()
    handlerCatchesException action = do
        action `E.catch` \(e :: SomeException) -> do
            liftIO $ putStrLn "typeOf:"
            liftIO $ print $ typeOf e
            liftIO $ putStrLn "====================================="
            liftIO $ putStrLn "Exception:"
            liftIO $ print e
            liftIO $ putStrLn "====================================="
        
    callingError :: Handler App App ()
    callingError = do
        error "And what if we fail calling 'error'?"

    patternMatchingError :: Handler App App ()
    patternMatchingError = do
        liftIO $ putStrLn "Will it print this?"
        let Just x = Nothing
        liftIO $ putStrLn x

    callingFail :: Handler App App ()
    callingFail = do
        fail "Fail this shit!!!"

    callingEmpty :: Handler App App ()
    callingEmpty = empty

