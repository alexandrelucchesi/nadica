{-# LANGUAGE BangPatterns               #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}

module Model.Order where

import           Control.Applicative
import           Control.Monad
import           Data.Aeson                           as A
import           Data.ByteString                      (ByteString)
import           Data.Char
import           Data.Int                             (Int64)
import           Data.Scientific                             (Scientific, toBoundedInteger)
import           Data.Text                            (Text)
import qualified Data.Text                            as T
import           Data.Time                   (UTCTime(..), secondsToDiffTime)
import           Data.Time.Calendar                   (Day, fromGregorian, toGregorian)
import           Data.Time.Format                     (formatTime, parseTime)
import           Database.PostgreSQL.Simple.FromField (FromField, fromField)
import           Database.PostgreSQL.Simple.ToField   (ToField, toField)
import           GHC.Generics
import           Snap.Snaplet.PostgresqlSimple        (FromRow (..), Postgres, ToRow,
                                                       field, toRow)
import Snap.Core (MonadSnap)
import           Snap.Snaplet
import           Application

import           System.Locale                        (defaultTimeLocale)
import           Model.Shirt
import           Model.User
import           Model.Misc
import  DB.Postgresql.DBUser (findUserById)
import  DB.Postgresql.DBShirt (listShirtsByOrderId)

data OrderStatus
    = Processing -- ^ After checkout and before payment.
    | Approved   -- ^ After payment is verified and before the product is shipped.
    | Shipping   -- ^ After the product is shipped.
    | Cancelled
  deriving (Eq, Show)

instance HasCode OrderStatus where
    fromCode 0 = Processing
    fromCode 1 = Approved 
    fromCode 2 = Shipping 
    fromCode 3 = Cancelled
    fromCode _ = error "Invalid status code."

    getCode Processing   = 0
    getCode Approved     = 1
    getCode Shipping     = 2
    getCode Cancelled    = 3

instance FromJSON OrderStatus where
    parseJSON = withScientific "OrderStatus" $ 
        maybe empty (pure . fromCode) . toBoundedInteger

instance ToJSON OrderStatus where
    toJSON = toJSON . getCode

instance FromField OrderStatus where
    fromField f dat = fromCode <$> fromField f dat

instance ToField OrderStatus where
    toField = toField . getCode


data Order = Order
    { orderId        :: Maybe Int64
    , orderUserId    :: Int64
    , orderCreatedAt :: UTCTime
    , orderStatus    :: OrderStatus
    }
  deriving (Eq, Generic, Show)

orderUser :: Order -> Handler App Postgres (Maybe User)
orderUser Order {orderUserId = uid} = findUserById uid

orderItems :: Order -> Handler App Postgres [Shirt]
orderItems Order {orderId = oid} = listShirtsByOrderId oid

instance FromJSON Order
instance ToJSON Order


defOrder :: Order
defOrder = Order
    { orderId        = Nothing
    , orderUserId    = -1
    , orderCreatedAt = UTCTime (fromGregorian 0 0 0) (secondsToDiffTime 0)
    , orderStatus    = Processing
    }

instance FromRow Order where
    fromRow = Order
        <$> _orderId        
        <*> _orderUserId      
        <*> _orderCreatedAt
        <*> _orderStatus    
      where
        !_orderId        = field
        !_orderUserId    = field
        !_orderCreatedAt = field
        !_orderStatus    = field

