<bind tag="custom-css">
  <link type="text/css" rel="stylesheet" href="/css/login.css" />
</bind>

<apply template="_base">
<div class="container" ng-controller="LoginCtrl" >
  <div class="page-header">
    <h1>Login</h1>
  </div>

  <!-- LOGIN ERROR -->
  <div ng-show="rspMsg" class="alert alert-dismissible center-block col-md-4" ng-class="{ 'alert-success' : ok, 'alert-danger' : !ok }" style="float: none;" role="alert">
    <button type="button" class="close" data-dismiss="alert">
      <span aria-hidden="true">&times;</span>
      <span class="sr-only">Close</span>
    </button>
    {{rspMsg}}
  </div><!-- ./LOGIN ERROR -->

  <div class="center-block col-md-4 form-wrapper">
    <form name="loginForm" class="" role="form" ng-submit="login(loginForm.$valid)" novalidate>
      <div class="form-group" ng-class="{ 'has-error' : loginForm.email.$invalid && submitted }">
        <label for="email" class="sr-only">E-mail</label>
        <input id="email" name="email" type="email" class="form-control" placeholder="E-mail" ng-model="email" required autofocus>
        <p ng-show="loginForm.email.$invalid && submitted" class="help-block">Digite um endereço de e-mail válido.</p>
      </div>
      <div class="form-group" ng-class="{ 'has-error' : loginForm.password.$invalid && submitted }">
        <label for="password" class="sr-only">Senha</label>
        <input id="password" name="password" type="password" class="form-control" placeholder="Senha" ng-model="password" ng-minlength="6" ng-maxlength="128" required>
        <p ng-show="loginForm.password.$invalid && submitted" class="help-block">Digite uma senha válida.</p>
      </div>
      <div class="row form-group">
        <div class="col-md-6">
          <div class="checkbox text-left">
            <label>
                <input type="checkbox" ng-model="remember"> Lembrar-me
            </label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="checkbox text-right">
            <a href="#">Esqueci a senha!</a>
          </div>
        </div>
      </div>
      <button class="btn btn-primary btn-block" type="submit">Entrar</button>
      <a href="/register" class="btn btn-default btn-block">Cadastrar</a>
    </form>
  </div>
</div>
</apply>

