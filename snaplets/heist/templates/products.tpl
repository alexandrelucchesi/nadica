<bind tag="selectedProduct">Adulto</bind> <!-- TODO: Set dinamically. -->
<apply template="_base">
<div class="container" ng-controller="ProductsCtrl">
  <div class="page-header">
    <h1>Camisetas: <selectedProduct /></h1>
  </div>

  <div class="row">
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Homem de Ferro</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Minions</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Capitão América</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Batman</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Homem de Ferro</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Minions</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Capitão América</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Batman</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Homem de Ferro</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Minions</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Capitão América</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="thumbnail">
        <a href="#">
          <img src="http://placehold.it/253x278" alt="..." />
        </a>
        <div class="caption">
          <h3>Batman</h3>
          <p>R$ 55,00</p>
          <p>
            <a href="#" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</a>
            <a href="#" class="btn btn-default" role="button"><span class="glyphicon glyphicon-eye-open"></span> Detalhes</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="text-center">
    <ul class="pagination">
      <li class="disabled">
        <a href="#">&laquo;
          <span class="sr-only">(Página atual)</span>
        </a>
      </li>
      <li class="active"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>  
      <li><a href="#">&raquo;</a></li>
    </ul>
  </div>

</div>
</apply>

