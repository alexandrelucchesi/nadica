{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TestOrder where

import           Application
import           Control.Exception             (SomeException)
import           Control.Applicative
import           Control.Monad                 (void)
import qualified Control.Monad.CatchIO         as E
import           Control.Monad.IO.Class        (liftIO)
import           Data.Int                      (Int64)
import qualified Data.Map                      as M
import           Data.Text                     (Text)
import           Data.Time.Calendar
import           Data.Typeable
import qualified Data.Map                      as M
import           DB.Postgresql.DBUser
import           DB.Postgresql.DBOrder
import           Model.User                    as User
import           Model.Order                    as Order
import           Site                          (app)
import           Snap.Snaplet
import qualified Snap.Snaplet.Auth             as SA
import           Snap.Snaplet.PostgresqlSimple
import           Snap.Snaplet.Test
import qualified Snap.Test                     as ST


testList :: IO (Either Text [Order])
testList =
    evalHandler Nothing (ST.get "" M.empty) (with db listOrders) app

