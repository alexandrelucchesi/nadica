{-# LANGUAGE BangPatterns               #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}

module Model.User where

import           Control.Applicative
import           Data.Aeson                           as A
import           Data.ByteString                      (ByteString)
import           Data.Char
import           Data.Int                             (Int64)
import           Data.Text                            (Text)
import qualified Data.Text                            as T
import           Data.Time.Calendar                   (Day, fromGregorian, toGregorian)
import           Data.Time.Format                     (formatTime, parseTime)
import           Database.PostgreSQL.Simple.FromField (FromField)
import           Database.PostgreSQL.Simple.ToField   (ToField)
import           GHC.Generics
import           Snap.Snaplet.PostgresqlSimple        (FromRow (..), field)
import           System.Locale                        (defaultTimeLocale)

-- -----------------------------------------------------------------------------
-- -- ** Data Types

-- | ISO/IEC 5218: 0 = not known, 1 = male, 2 = female, 9 = not applicable.
type Gender = Int

unknown, male, female, notApplicable :: Gender
unknown       = 0
male          = 1
female        = 2
notApplicable = 9

isUnknown, isMale, isFemale, isNotApplicable :: Gender -> Bool
isUnknown       = (==) 0
isMale          = (==) 1
isFemale        = (==) 2
isNotApplicable = (==) 9

newtype Date = Date Day
  deriving (Eq, Show, FromField, ToField)

data User = User
    { userId         :: Maybe Int64
    , userName       :: Text
    , userSurname    :: Text
    , userBirthday   :: Date
    , userGender     :: Gender
    , userRg         :: Text
    , userCpf        :: Text
    , userPhone      :: Text
    , userCountry    :: Text
    , userState      :: Text
    , userCity       :: Text
    , userDistrict   :: Text
    , userAddress    :: Text
    , userComplement :: Text -- ^ Optional.
    , userNumber     :: Text
    , userZipCode    :: Text
    , userEmail      :: Text
    , userPassword   :: Text
    }
  deriving (Eq, Generic, Show)

------------------------------------------------------------------------------
-- | Default User that has all empty values.
defUser :: User
defUser = User
    { userId         = Nothing
    , userName       = ""
    , userSurname    = ""
    , userBirthday   = Date (fromGregorian 0 0 0)
    , userGender     = unknown
    , userRg         = ""
    , userCpf        = ""
    , userPhone      = ""
    , userCountry    = "Brasil"
    , userState      = ""
    , userCity       = ""
    , userDistrict   = ""
    , userAddress    = ""
    , userComplement = ""
    , userNumber     = ""
    , userZipCode    = ""
    , userEmail      = ""
    , userPassword   = ""
    }

instance FromJSON Date where
  parseJSON = withText "Day" $ \t ->
    case parseTime defaultTimeLocale "%F" (T.unpack t) of
      Just d -> pure $ Date d
      _      -> empty

instance ToJSON Date where
  toJSON (Date d) = String $ T.pack $ formatTime defaultTimeLocale "%F" d

instance FromJSON User
instance ToJSON User

-- | For reading an user, perform a query "inner joining" tb_user and
-- snap_auth_user as (the order of columns in "SELECT" matters):
--
--    SELECT u.*, a.login, a.password FROM
--      tb_user u INNER JOIN snap_auth_user a ON u.uid = a.uid
--
-- See this link for explanation of why the identifiers are preceded by an
-- underscore:
-- http://stackoverflow.com/questions/12746229/what-does-it-usually-mean-when-a-haskell-record-accessor-leads-with-an-underscor
--
-- The exclamation mark is related to the extension "BangPatterns" and forces
-- the evaluation of a value to the WNHF.
instance FromRow User where
    fromRow = User
        <$> _userId
        <*> _userName
        <*> _userSurname
        <*> _userBirthday
        <*> _userGender
        <*> _userRg
        <*> _userCpf
        <*> _userPhone     
        <*> _userCountry   
        <*> _userState     
        <*> _userCity      
        <*> _userDistrict  
        <*> _userAddress   
        <*> _userComplement
        <*> _userNumber    
        <*> _userZipCode   
        <*> _userEmail     
        <*> _userPassword  
      where
        !_userId         = field
        !_userName       = field
        !_userSurname    = field
        !_userBirthday   = field
        !_userGender     = field
        !_userRg         = field
        !_userCpf        = field
        !_userPhone      = field
        !_userCountry    = field
        !_userState      = field
        !_userCity       = field
        !_userDistrict   = field
        !_userAddress    = field
        !_userComplement = field
        !_userNumber     = field
        !_userZipCode    = field
        !_userEmail      = field <|> pure "" -- 'email' and 'password' can be obtained
        !_userPassword   = field <|> pure "" -- from the 'snap_auth_user' table.

-- -----------------------------------------------------------------------------
-- -- ** Type Synonyms

type Field        = ByteString
type ErrorMessage = Text
type ErrorList = [(Field, ErrorMessage)]

-- -----------------------------------------------------------------------------
-- -- ** Validation

type Valid a = Either ErrorMessage a
type Normalized a = a

-- TODO: Change to validateUser :: Date -> Validate User
-- | Validates an user, which is valid if this function returns [].
validateUser :: Date -> User -> [(Field, ErrorMessage)]
validateUser today User{..} = concat
    [ validateName userName
    , validateSurname userSurname
    , validateBirthday today userBirthday
    , validateGender userGender
    , validateRG userRg
    , validateCPF userCpf
    , validatePhone userPhone
    , validateCountry userCountry
    , validateState userState
    , validateCity userCity
    , validateDistrict userDistrict
    , validateAddress userAddress
    , validateComplement userComplement
    , validateNumber userNumber
    , validateZipCode userZipCode
    , validateEmail userEmail
    , validatePassword userPassword
    ]

validateName :: Text -> [(Field, ErrorMessage)]
validateName name
  | T.length name > 0 && T.length name < 50
    && T.all (\c -> isAlpha c || isSpace c) name = []
  | otherwise = [("name", "Nome inválido.")]

validateSurname :: Text -> [(Field, ErrorMessage)]
validateSurname surname
  | T.length surname > 0 && T.length surname < 100
    && T.all (\c -> isAlpha c || isSpace c) surname = []
  | otherwise = [("surname", "Sobrenome inválido.")]

validateBirthday :: Date -> Date -> [(Field, ErrorMessage)]
validateBirthday (Date today) (Date birthday) =
  let (year1,_,_) = toGregorian today
      (year2,_,_) = toGregorian birthday
  in if year1 - year2 < 123
       then []
       else [("birthday", "Data de nascimento inválida.")]

validateGender :: Gender -> [(Field, ErrorMessage)]
validateGender gender
  | isMale gender || isFemale gender = []
  | otherwise = [("gender", "Sexo inválido.")]

validateRG :: Text -> [(Field, ErrorMessage)]
validateRG rg
  | T.length rg <= 20 = []
  | otherwise = [("rg", "RG inválido.")]

validateCPF :: Text -> [(Field, ErrorMessage)]
validateCPF cpf
  | T.length cpf == 11 && T.all isNumber cpf =
      let cpfNum  = T.foldr (\c xs -> digitToInt c : xs) [] cpf
          begin1  = take 9 cpfNum
          result1 = sum $ zipWith (*) begin1 [10,9..2]
          num1    = let rem1 = result1 `mod` 11
                    in if rem1 < 2 then 0 else 11 - rem1
          begin2  = begin1 ++ [num1]
          result2 = sum $ zipWith (*) begin2 [11,10..2]
          num2    = let rem2 = result2 `mod` 11
                    in if rem2 < 2 then 0 else 11 - rem2
      in if num1 == cpfNum !! 9 && num2 == cpfNum !! 10
           then []
           else err
  | otherwise = err
  where err = [("cpf", "CPF inválido.")]

validatePhone :: Text -> [(Field, ErrorMessage)]
validatePhone phone
  | T.all isNumber phone
    && (T.length phone == 11 && "119" `T.isPrefixOf` phone
       || T.length phone == 10) = []
  | otherwise = [("phone", "Telefone inválido.")]

validateCountry :: Text -> [(Field, ErrorMessage)]
validateCountry country
  | country == "Brasil" = []
  | otherwise = [("country", "País inválido.")]

validateState :: Text -> [(Field, ErrorMessage)]
validateState state
  | state `elem` states = []
  | otherwise = [("state", "Estado inválido.")]
  where
    states = [ "AC", "AL", "AP", "AM", "BA", "CE", "DF", "GO", "ES",
               "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI",
               "RJ", "RN", "RS", "RO", "RR", "SP", "SC", "SE", "TO" ]

validateCity :: Text -> [(Field, ErrorMessage)]
validateCity city
  | T.length city > 0 && T.length city < 50
    && T.all (\c -> isAlphaNum c || isSpace c || c `elem` ".") city = []
  | otherwise = [("city", "Cidade inválida.")]

validateDistrict :: Text -> [(Field, ErrorMessage)]
validateDistrict district
  | T.length district > 0 && T.length district < 50
    && T.all (\c -> isAlphaNum c || isSpace c || c `elem` ".") district = []
  | otherwise = [("district", "Bairro inválido.")]

validateAddress :: Text -> [(Field, ErrorMessage)]
validateAddress address
  | T.length address > 0 && T.length address < 100
    && T.all (\c -> isAlphaNum c || isSpace c || c `elem` ".,/") address = []
  | otherwise = [("address", "Endereço inválido.")]

validateComplement :: Text -> [(Field, ErrorMessage)]
validateComplement complement
  | T.length complement < 100
    && T.all (\c -> isAlphaNum c || isSpace c || c `elem` ".,/") complement = []
  | otherwise = [("complement", "Complemento inválido.")]

validateNumber :: Text -> [(Field, ErrorMessage)]
validateNumber number
  | T.length number < 10
    && T.all (\c -> isAlphaNum c || isSpace c || c `elem` ".,") number = []
  | otherwise = [("number", "Número inválido.")]

validateZipCode :: Text -> [(Field, ErrorMessage)]
validateZipCode zipCode -- TODO: Implement validation with some Web service.
  | T.length zipCode == 8 && T.all isDigit zipCode = []
  | otherwise = [("zipCode", "CEP inválido.")]

validateEmail :: Text -> [(Field, ErrorMessage)]
validateEmail email
  | T.length email > 0 && T.length email < 100 && T.count "@" email == 1
    && T.all (\c -> isAlphaNum c || c `elem` ".@") email = []
  | otherwise = [("email", "E-mail inválido.")]

validatePassword :: Text -> [(Field, ErrorMessage)]
validatePassword password
  | T.length password >= 6 && T.length password <= 128
    && T.all isAscii password = []
--  | T.length password >= 6 && T.length password <= 128 && T.all isAscii password
--    && T.any isUpper password && T.any isLower password && T.any isDigit password
--    && T.any (`elem` " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~") password = []
  | otherwise = [("password", "Senha inválida.")]

