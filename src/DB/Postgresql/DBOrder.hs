{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DB.Postgresql.DBOrder
    ( listOrders
    ) where

import           Application
import           Control.Applicative                ((<$>))
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Int                           (Int64)
import           Data.Maybe                         (fromJust, isJust,
                                                     isNothing, listToMaybe)
import           Data.Text                          (Text)
import qualified Data.Text                          as T
import           Data.Text.Encoding                 as T (encodeUtf8)
import           Data.Text.Read                     (decimal)
import           Data.Time                          (getCurrentTime, utctDay)
import qualified Database.PostgreSQL.Simple.ToField as P
import           Database.PostgreSQL.Simple.Types   (Query (..))

import           Model.User
import           Model.Order
import           Snap.Snaplet
import qualified Snap.Snaplet.Auth                  as SA
import           Snap.Snaplet.PostgresqlSimple


-- -----------------------------------------------------------------------------
-- -- ** Data Types
-- | Datatype containing the names of the columns for the users table.
data OrderTable =  OrderTable
    {  orderTblName :: Text
    ,  colOrderId   :: Text
    ,  colUserId    :: Text
    ,  colCreatedAt :: Text
    ,  colStatus    :: Text
    }


defOrderTable :: OrderTable
defOrderTable = OrderTable
    {  orderTblName = "tb_order"
    ,  colOrderId   = "order_id"
    ,  colUserId    = "user_id"
    ,  colCreatedAt = "created_at"
    ,  colStatus    = "status"
    }


-- | List of deconstructors so it's easier to extract column names from an
-- 'UserTable'.
orderColDef :: [(OrderTable -> Text, Order -> P.Action)]
orderColDef =
    [ (colOrderId   , P.toField . orderId)
    , (colUserId    , P.toField . orderUserId)
    , (colCreatedAt , P.toField . orderCreatedAt)
    , (colStatus    , P.toField . orderStatus)
    ]


------------------------------------------------------------------------------
-- | CRUD

-- | Builds the 'create query'. When inserting an user, the user id which
-- references the 'snap_auth_user' table MUST be specified.
createQuery :: OrderTable -> Order -> (Text, [P.Action])
createQuery ot o@Order{..} =
    (T.concat [ "INSERT INTO "
              , orderTblName ot
              , " ("
              , T.intercalate "," cols
              , ") VALUES ("
              , T.intercalate "," vals
              , ") RETURNING "
              , T.intercalate "," cols
              ]
              , params)
  where
    cols = map (($ot) . fst) orderColDef
    vals = map (const "?") cols
    params = map (($o) . snd) orderColDef


-- | Builds 'find queries'. If the 3rd parameter is 'Nothing', returns a 'select
-- all' query.
findQuery :: OrderTable -> Maybe Int64 -> (Text, [P.Action])
findQuery OrderTable{..} maybeOrderId =
    case maybeOrderId of
        Just orderId ->
            (T.concat [ selectQuery
                      , " WHERE "
                      , colOrderId, " = ?"
                      ]
            , [P.toField orderId])
        _ -> (selectQuery, [])
  where
    selectQuery =
        T.concat [ "SELECT * FROM "
                 , orderTblName
                 ]


-- | Lists all users.
listOrders :: Handler App Postgres [Order]
listOrders = do
    let (qstr, _) = findQuery defOrderTable Nothing
        q = Query $ T.encodeUtf8 qstr
    query_ q


