{-# LANGUAGE BangPatterns               #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}

module Model.Shirt where

import           Control.Applicative
import           Data.Aeson                           as A
import           Data.Int                             (Int64)
import           Data.Text                            (Text)
import           GHC.Generics
import           Snap.Snaplet.PostgresqlSimple        (FromRow (..), field)


data Shirt = Shirt
    { shirtId          :: Maybe Int64
    , shirtStamp       :: Text
    , shirtDescription :: Text          -- ^ Optional.
    , shirtColor       :: Text
    , shirtPrice       :: Float
    , shirtInventory   :: Int
    }
  deriving (Eq, Generic, Show)

instance FromJSON Shirt
instance ToJSON Shirt

------------------------------------------------------------------------------
-- | Default Shirt that has all empty values.
defShirt :: Shirt
defShirt = Shirt
    { shirtId          = Nothing
    , shirtStamp       = ""
    , shirtDescription = ""
    , shirtColor       = ""
    , shirtPrice       = 0.0
    , shirtInventory   = 0
    }


instance FromRow Shirt where
    fromRow = Shirt
        <$> _shirtId         
        <*> _shirtStamp      
        <*> _shirtDescription
        <*> _shirtColor      
        <*> _shirtPrice      
        <*> _shirtInventory  
      where
        !_shirtId          = field
        !_shirtStamp       = field
        !_shirtDescription = field
        !_shirtColor       = field
        !_shirtPrice       = fromRational <$> field
        !_shirtInventory   = field

